/**
 * @Author: Luis Valdez,Bastien Nisole,Patrick Zhuang
 * @Date:   2018-11-09T23:44:52+01:00
 * @Email:  patrickzhuang@hotmail.fr
 * @Project: Linter-c
 * @Filename: integrity.h
 * @Last modified by:   Luis Valdez
 * @Last modified time: 2018-12-11T02:59:41+01:00
 */


#ifndef __integrity_h__
#define __integrity_h__

#include "headers.h"

#define RULES_STEP         1
#define RECURSIVE_STEP     2
#define EXCLUDEDFILES_STEP 3
#define EXTENDS_STEP       4
#define NO_STEP            -1
#define TRUE               1
#define FALSE              0
int checkIntegrity(FILE * confFile);
int checkfilePathIntegrity(char * path);
int checkRulesIntegrity(char * rule);
int checkRecursiveIntegrity(char * value);

#endif /* ifndef __integrity_h__ */
