/**
 * @Author: Luis Valdez,Bastien Nisole,Patrick Zhuang
 * @Date:   2018-12-09T19:18:29+01:00
 * @Email:  patrickzhuang@hotmail.fr
 * @Project: Linter-c
 * @Filename: noMultipleDeclaration.h
 * @Last modified by:   Luis Valdez
 * @Last modified time: 2018-12-11T02:59:46+01:00
 */


#ifndef NOMULTIPLEDECLARATION_H_INCLUDED
#define NOMULTIPLEDECLARATION_H_INCLUDED

int checkMultipleDeclaration(struct Line * line, char * filename);
char * nothingInBracketOrParenthesis(char * string);

#endif
