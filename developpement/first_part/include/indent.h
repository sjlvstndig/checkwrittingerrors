/**
 * @Author: Luis Valdez,Bastien Nisole,Patrick Zhuang
 * @Date:   2018-11-21T18:48:18+01:00
 * @Email:  luis@valdez.fr
 * @Project: Linter-c
 * @Filename: indent.h
 * @Last modified by:   Luis Valdez
 * @Last modified time: 2018-12-11T02:59:37+01:00
 */


#ifndef __indent_h__
#define __indent_h__

#include "headers.h"

int checkIndent(Line * line, int * currentIndent, int increment, int * diffParenthesis);
int indentIncrement(int currentValue, int increment, int diff);
char checkClosedBracketAlone(Line * string, int res, int currentIndent, int increment);
int initParenthesisCount(int diffParenthesis, int * parenthesisOpened, int * parenthesisClosed);
#endif
