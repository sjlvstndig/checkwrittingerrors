/**
 * @Author: Luis Valdez,Bastien Nisole,Patrick Zhuang
 * @Date:   2018-11-21T17:49:41+01:00
 * @Email:  patrickzhuang@hotmail.fr
 * @Project: Linter-c
 * @Filename: utils.h
 * @Last modified by:   Luis Valdez
 * @Last modified time: 2018-12-11T02:59:55+01:00
 */


#ifndef UTILS_H_INCLUDED
#define UTILS_H_INCLUDED

#define NORMAL         0
#define IN_COMMENT     1
#define EMPTY_STRING   2
#define FALSE_NEXT_TWO 3
#define FALSE_NEXT     4

#include "headers.h"

typedef struct Line Line;
struct Line {
    int    lineNumber;
    int    indentValue;
    char * lineValue;
};


char * cpyString(char * s);
void trimString(char * s);
void writeInFile(char * filename, char * outputString);
void writeErrorFile(char * outputString);
void printErrors();
void printCurrentRules(Linter * linter);
void removeHardString(char * s);
Line * getASingleLine(FILE * file, int lineNumber, char * isComment);
void checkBeginComment(char currentchar, FILE * file, char * isComment);
void checkEndComment(char currentchar, FILE * file, char * isComment);
char * initLine(FILE * file);
char * removeComment(char * currentString, long size);
void noCommentFile(FILE * file);


// #include "headers.h"
//
// typedef struct Line Line;
// struct Line {
//     int    lineNumber;
//     int    indentValue;
//     char * lineValue;
// };
//
// void trimString(char * s);
// void writeInFile(char * filename, char * outputString);
// void printCurrentRules(Linter * linter);
// Line * getASingleLine(FILE * file, int lineNumber);
// char * cpyString(char * s);
// void removeHardString(char * s);
// char * removeComment(char * currentString, long size);
// void noCommentFile(FILE * file);
#endif // UTILS_H_INCLUDED
