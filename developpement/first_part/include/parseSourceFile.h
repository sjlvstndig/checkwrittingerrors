/**
 * @Author: Luis Valdez
 * @Date:   2018-12-09T13:36:48+01:00
 * @Email:  luis@valdez.fr
 * @Project: Linter-c
 * @Filename: parseSourceFile.h
 * @Last modified by:   Luis Valdez
 * @Last modified time: 2018-12-11T02:59:49+01:00
 */


#ifndef __parseSourceFile_h__
#define __parseSourceFile_h__
#include "headers.h"

char prototypeExist(Functions * funcs, FILE * f, char * filename);
char * startFunc(char * str, char ** pos);
char * checkProto(char * str, char * pos, Function * func);
char * getTypeProto(char * str, char * namePos);
char * getProtoParams(char * pos);

Function * getFunction(FILE * source);
char * getContentFunction(FILE * source, int start, int end);
char * parseParams(char * params);
char * getParams(FILE * source, int  * position);
char * getName(char * header);
char * getType(char * type);
char * getFuncName(FILE * source, int  * position);
char * getTypeFunc(FILE * source, int  * position);
FILE * cpyNofunc(FILE * source, Functions * arrayFunc);
char WriteOrNot(int pos, Functions * arrayFunc);

int parseFuncFiles(Linter * linter);
int parseFunctions(FILE * f, char * filename);
#endif /* ifndef __parseSourceFile_h__ */
