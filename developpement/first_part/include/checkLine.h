/**
 * @Author: Luis Valdez,Bastien Nisole,Patrick Zhuang
 * @Date:   2018-11-21T17:49:41+01:00
 * @Email:  patrickzhuang@hotmail.fr
 * @Project: Linter-c
 * @Filename: checkLine.h
 * @Last modified by:   Luis Valdez
 * @Last modified time: 2018-12-11T02:59:09+01:00
 */

#ifndef CHECKLINE_H_INCLUDED
#define CHECKLINE_H_INCLUDED

#define RULE_RESPECTED     4
#define RULE_NOT_RESPECTED 5

#define NO_HEADER          0
#define HAS_HEADER         1
#define IN_CHECK           2
#define CHECKED            3
#define HEADER_ERROR       4

int checkWrittingRules(Linter * linter);
int browseFile(FILE * file, char *  filename, Linter * linter);
int checkASingleLine(Linter * linter, Line * line, char * filename, char * hasHeader, char lineType,
  int * currentIndent, int * diffParenthesis);
int checkFileMax(int maxFileLineNumbers, int count);
int checkLineMax(int maxLineNumbers, Line * line);
int checkNoTrailingSpaces(Line * line);
void checkBeginComment(char currentchar, FILE * file, char * lineType);
void checkEndComment(char currentchar, FILE * file, char * lineType);
char * initLine(FILE * file);
void checkInLineComment(Line * returnedLine);
char checkHeader(char lineType, char hasHeader);

#endif // CHECKLINE_H_INCLUDED
