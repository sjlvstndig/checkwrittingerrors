/**
 * @Author: Luis Valdez,Bastien Nisole,Patrick Zhuang
 * @Date:   2018-11-09T23:44:52+01:00
 * @Email:  patrickzhuang@hotmail.fr
 * @Project: Linter-c
 * @Filename: getConfigRules.h
 * @Last modified by:   Luis Valdez
 * @Last modified time: 2018-12-11T02:59:28+01:00
 */


#ifndef __mainEventHandler_h__
#define __mainEventHandler_h__

#include "headers.h"

#define ERROR   0
#define SUCCESS 1
Linter * getConfig();
int recursiveSearchExtends(char * confDirectoryName, char * filename, Linter * linter, ArrayInfo * excludedFiles);


#endif /* ifndef __mainEventHandler_h__ */
