/**
 * @Author: Luis Valdez
 * @Date:   2018-12-09T20:51:08+01:00
 * @Email:  luis@valdez.fr
 * @Project: Linter-c
 * @Filename: functions.h
 * @Last modified by:   Luis Valdez
 * @Last modified time: 2018-12-11T02:59:25+01:00
 */

#ifndef __FUNCTIONS_h__
#define __FUNCTIONS_h__

typedef struct Function Function;
typedef struct Functions Functions;

struct Function {
    char * type;
    char * name;
    char * content;
    char * paramsString;
    int    initPosition;
    int    endPosition;
};

struct Functions {
    Function ** funcs;
    int         size;
};

Function * newFunc();
Functions * newArrayFunc();
Function * destroyFunc(Function * func);
Functions * destroyArrayFunc(Functions * func);
int addFunc(Functions * funcs, Function * value);
Function * cpyFunc(Function * value);

#endif /* ifndef __FUNCTIONS_h__ */
