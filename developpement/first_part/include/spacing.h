/**
 * @Author: Luis Valdez,Bastien Nisole,Patrick Zhuang
 * @Date:   2018-12-09T19:18:29+01:00
 * @Email:  patrickzhuang@hotmail.fr
 * @Project: Linter-c
 * @Filename: spacing.h
 * @Last modified by:   Luis Valdez
 * @Last modified time: 2018-12-11T02:59:52+01:00
 */


#ifndef SPACING_H_INCLUDED
#define SPACING_H_INCLUDED

typedef struct tree Tree;
struct tree {
    int    val;
    Tree * brother;  // Indiquer le frère de l'element
    Tree * children; // Indiquer l'un de ses enfants
};

int checkCommaSpacing(Line * line, char * filename);
char * checkSpace(char * string, char *  operator, int lineNumber, char * filename);
int travelTree(Tree * node, char *  operator, char **  string, int lineNumber, char * filename, Tree * first);
int doTmp(Line * line, char * filename);


#endif /* ifndef SPACING_H_INCLUDED */
