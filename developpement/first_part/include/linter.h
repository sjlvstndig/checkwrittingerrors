/**
 * @Author: Luis Valdez,Bastien Nisole,Patrick Zhuang
 * @Date:   2018-11-01T12:10:49+01:00
 * @Email:  luis@valdez.fr
 * @Project: Linter-c
 * @Filename: linter.h
 * @Last modified by:   Luis Valdez
 * @Last modified time: 2018-12-11T02:59:44+01:00
 */


#ifndef __linter_h__
#define __linter_h__

#include "headers.h"

typedef struct Rules Rules;
typedef struct Linter Linter;
typedef struct Line Line;

struct Rules {
    char arrayBracketEol;
    char operatorsSpacing;
    char commaSpacing;
    int  indent;
    char commentsHeader;
    int  maxLineNumbers;
    int  maxFileLineNumbers;
    char noTrailingSpaces;
    char noMultiDeclaration;
    char unusedVariable;
    char undeclaredVariable;
    char noPrototype;
    char unusedFunction;
    char undeclaredFunction;
    char variableAssignmentType;
    char functionParametersType;
};

struct Linter {
    char        recursive;
    ArrayInfo * includedFiles;
    Rules *     rules;
};


Linter * newLinter();
int destroyLinter(Linter * linter);

#endif /* ifndef __linter_h__ */
