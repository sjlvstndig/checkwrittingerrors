/**
 * @Author: Luis Valdez
 * @Date:   2018-12-04T22:12:59+01:00
 * @Email:  luis@valdez.fr
 * @Project: Linter-c
 * @Filename: headers.h
 * @Last modified by:   Luis Valdez
 * @Last modified time: 2018-12-11T02:59:34+01:00
 */


#ifndef HEADERS_H_INCLUDED
#define HEADERS_H_INCLUDED

#define ERROR   0
#define SUCCESS 1

#define OFF     -1
#define ON      1

#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "arrayInfo.h"
#include "linter.h"
#include "utils.h"
#include "getIncludeFiles.h"
#include "getConfigRules.h"
#include "configLinter.h"
#include "integrity.h"
#include "indent.h"
#include "checkBracket.h"
#include "checkLine.h"
#include "spacing.h"
#include "noMultipleDeclaration.h"
#include "functions.h"
#include "parseSourceFile.h"


#endif // HEADERS_H_INCLUDED
