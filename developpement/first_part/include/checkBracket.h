/**
 * @Author: Luis Valdez,Bastien Nisole,Patrick Zhuang
 * @Date:   2018-11-21T23:13:13+01:00
 * @Email:  luis@valdez.fr
 * @Project: Linter-c
 * @Filename: checkBracket.h
 * @Last modified by:   Luis Valdez
 * @Last modified time: 2018-12-11T02:59:06+01:00
 */


#ifndef CHECKBRACKET_H_INCLUDED
#define CHECKBRACKET_H_INCLUDED
#include "headers.h"
int checkBracket(Line * line);
#endif // CHECKBRACKET_H_INCLUDED
