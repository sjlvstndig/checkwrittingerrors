/**
 * @Author: Luis Valdez,Bastien Nisole,Patrick Zhuang
 * @Date:   2018-12-09T19:18:29+01:00
 * @Email:  patrickzhuang@hotmail.fr
 * @Project: Linter-c
 * @Filename: getIncludeFiles.h
 * @Last modified by:   Luis Valdez
 * @Last modified time: 2018-12-11T02:59:31+01:00
 */


#ifndef __getIncludeFiles_h__
#define __getIncludeFiles_h__

#include "headers.h"

// read conf files and get exluded files
char ** getExcludeFiles(FILE * confFile, ArrayInfo * excludedFiles);
// main event get included files
int getIncludeFilesHandler(Linter * linter, ArrayInfo * excludedFiles);
// Print string array
void printArray(char ** array, int size);
// Free a 2d array
void free2dArray(char ** array, int size);
// Allocate a 2d array of 1 row
void alloc(char *** array);
// Reallocate a 2D array
void realloc2DArray(char *** ptr, int size);
// Check is the file is banned
int isOk(char * str, ArrayInfo * excludedFiles);
// Return file to read
ArrayInfo * recu(char * str, char recursif, ArrayInfo * fileChecked, ArrayInfo * excludedFiles);


#endif /* ifndef __getIncludeFiles_h__ */
