/**
 * @Author: Luis Valdez,Bastien Nisole,Patrick Zhuang
 * @Date:   2018-12-09T19:18:29+01:00
 * @Email:  patrickzhuang@hotmail.fr
 * @Project: Linter-c
 * @Filename: arrayInfo.h
 * @Last modified by:   Luis Valdez
 * @Last modified time: 2018-12-11T02:59:01+01:00
 */


#ifndef __ArrayInfo_h__
#define __ArrayInfo_h__

typedef struct ArrayInfo {
    char ** fileToCheck;
    int     size;
} ArrayInfo;

#endif
