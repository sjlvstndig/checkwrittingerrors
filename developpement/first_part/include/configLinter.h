/**
 * @Author: Luis Valdez,Bastien Nisole,Patrick Zhuang
 * @Date:   2018-11-01T12:10:49+01:00
 * @Email:  luis@valdez.fr
 * @Project: Linter-c
 * @Filename: configLinter.h
 * @Last modified by:   Luis Valdez
 * @Last modified time: 2018-12-11T02:59:22+01:00
 */


#ifndef __config_h__
#define __config_h__

#define RULES_HEADER         "=rules"
#define EXTENDS_HEADER       "=extends"
#define EXCLUDEDFILES_HEADER "=excludedFiles"
#define RECURSIVE_HEADER     "=recursive"

#define OFF                  -1
#define ON                   1

#define TRUE_STRING          "true"
#define FALSE_STRING         "false"
#define INTEGER_VALUE        0
#define NOT_INTEGER_VALUE    1

#include "headers.h"


int getRules(FILE * source, Linter * linter);
int getValue(char * value, char type);
int identifyRule(char * rule, Linter * linter);
int getRecursiveConfig(FILE * conf, Linter * linter);

#endif /* ifndef __config_h__ */
