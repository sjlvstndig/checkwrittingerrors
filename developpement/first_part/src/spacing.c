/**
 * @Author: Luis Valdez,Bastien Nisole,Patrick Zhuang
 * @Date:   2018-11-22T17:52:48+01:00
 * @Email:  luis@valdez.fr
 * @Project: Linter-c
 * @Filename: spacing.c
 * @Last modified by:   Luis Valdez
 * @Last modified time: 2018-12-11T03:00:34+01:00
 */


#include "headers.h"

/**
 *  Check comma spacing
 *
 * @method checkCommaSpacing
 *
 * @param  line          Line to check
 *         filename      file name
 *
 * @return               return 1 if success
 */
int checkCommaSpacing(Line * line, char * filename) {
    int i;
    char tempString[500];

    for (i = 0; i < strlen(line->lineValue); i++) {
        if (line->lineValue[i] == ',') {
            if (line->lineValue[i + 1] != ' ') {
                sprintf(tempString, "[%s : %d] Il manque un espace a droite de votre virgule.\n", filename,
                  line->lineNumber);
                writeErrorFile(tempString);
            } else if (line->lineValue[i - 1] == ' ' || line->lineValue[i + 2] == ' ') {
                sprintf(tempString, "[%s : %d] Votre virgule est mal ecrite (\"X, X\").\n", filename, line->lineNumber);
                writeErrorFile(tempString);
            }
        }
    }
    return 1;
}

/**
 *  check spacing between operators
 *
 * @method checkSpace
 *
 * @param  string        string in what we are looking for operator
 *         operator      string of current operator
 *         lineNumber    Number of current line
 *         filename      current file name
 *
 * @return               return string without finded operator
 */
char * checkSpace(char * string, char *  operator, int lineNumber, char * filename) {
    int i, y, j;
    char * tmp;
    char tempString[500];

    if (strstr(operator, "++") != NULL) {
        if ( (string[strstr(string,
          operator) - string - 1] != ' ' &&
          string[strstr(string,
          operator) - string + strlen(operator)] == ' ' ) ||
          (string[strstr(string,
          operator) - string - 1] == ' ' &&
          string[strstr(string, operator) - string + strlen(operator)] != ' ' ) )
        { } else {
            sprintf(tempString, "[%s : %d] Le caractere '++' est mal ecrit.\n", filename, lineNumber);
            writeErrorFile(tempString);
        }
    } else if (strstr(operator, "--") != NULL) {
        if ( (string[strstr(string,
          operator) - string - 1] != ' ' &&
          string[strstr(string,
          operator) - string + strlen(operator)] == ' ' ) ||
          (string[strstr(string,
          operator) - string - 1] == ' ' &&
          string[strstr(string, operator) - string + strlen(operator)] != ' ' ) )
        { } else {
            sprintf(tempString, "[%s : %d] Le caractere '--' est mal ecrit.\n", filename, lineNumber);
            writeErrorFile(tempString);
        }
    } else if (strstr(operator, "->") != NULL) {
        // Do something
    } else if (strstr(operator, "*") != NULL) {
        i = 1;
        while (string[strstr(string, operator) - string + i++ ] == '*');

        realloc(operator, i);
        for (j = 0; j < i - 1; j++) {
            operator[j] = '*';
        }
        operator[j] = '\0';
        if (string[strstr(string,
          operator) - string + strlen(operator)] != ' ' || string[strstr(string, operator) - string - 1] != ' ')
        {
            sprintf(tempString, "[%s : %d] Le caractere '%s' est mal ecrit.\n", filename, lineNumber, operator);
            writeErrorFile(tempString);
        }
    } else if (string[strstr(string,
      operator) - string - 1] != ' ' || string[strstr(string, operator) - string + strlen(operator)] != ' ')
    {
        sprintf(tempString, "[%s : %d] Le caractere '%s' est mal ecrit.\n", filename, lineNumber, operator);
        writeErrorFile(tempString);
    }
    // ***********
    // Delete verified Char
    // ***********

    tmp = malloc(strlen(string) - strlen(operator) + 1);
    if (tmp == NULL)
        return NULL;

    for (i = 0; i < strstr(string, operator) - string; i++) {
        tmp[i] = string[i];
    }
    y = i;
    for (i += strlen(operator); i < strlen(string); i++) {
        tmp[ y++ ] = string[i];
    }
    tmp[y] = '\0';
    return tmp;
} /* checkSpace */

/**
 *  travel a tree of operator
 *
 * @method travelTree
 *
 * @param  node          current node of the tree
 *         operator      string of current operator
 *         string        string in what we are looking for operator
 *         lineNumber    Number of current line
 *         filename      current file name
 *         first         first node of the tree
 *
 * @return               return 1 if success or ERROR if malloc error
 */

int travelTree(Tree * node, char *  operator, char **  string, int lineNumber, char * filename, Tree * first) {
    char * tmp;
    int res = -1;

    if (node->children != NULL) {
        tmp = malloc(sizeof(char) * (strlen(operator) + 2));
        if (tmp == NULL)
            return ERROR;

        sprintf(tmp, "%s%c", operator, node->val);
        travelTree(node->children, tmp, string, lineNumber, filename, first);
        free(tmp);
    } else {
        if (strstr(*string, operator) != NULL) {
            *string = checkSpace(*string, operator, lineNumber, filename);
            res     = 1;
        }
    }

    if (res == 1) {
        strcpy(operator, "");
        travelTree(first, operator, string, lineNumber, filename, first);
    } else if (node->brother != NULL) {
        travelTree(node->brother, operator, string, lineNumber, filename, first);
    }
    return SUCCESS;
}

/**
 *  create a temporary string from the last one and create to tree of operator
 *
 * @method doTmp
 *
 * @param  line          current line
 *         filename      current file name
 *
 * @return               return 1 if success or NULL if malloc error
 */
int doTmp(Line * line, char * filename) {
    Tree Z = { '\0', NULL, NULL };

    Tree M   = { '~', NULL, &Z };
    Tree LA  = { '=', &Z, &Z };
    Tree L   = { '^', &M, &LA };
    Tree KB  = { '=', &Z, &Z };
    Tree KA  = { '|', &KB, &Z };
    Tree K   = { '|', &L, &KA };
    Tree JB  = { '=', &Z, &Z };
    Tree JA  = { '&', &JB, &Z };
    Tree J   = { '&', &K, &JA };
    Tree IA  = { '=', &Z, &Z };
    Tree I   = { '=', &J, &IA };
    Tree HA  = { '=', &Z, &Z };
    Tree H   = { '!', &I, &HA };
    Tree GB  = { '=', &Z, &Z };
    Tree GAB = { '=', NULL, &Z };
    Tree GA  = { '>', &GB, &GAB };
    Tree G   = { '>', &H, &GA };
    Tree FB  = { '=', &Z, &Z };
    Tree FAB = { '=', NULL, &Z };
    Tree FA  = { '<', &FB, &FAB };
    Tree F   = { '<', &G, &FA };
    Tree EA  = { '=', &Z, &Z };
    Tree E   = { '%', &F, &EA };
    Tree DA  = { '=', &Z, &Z };
    Tree D   = { '/', &E, &DA };
    Tree CC  = { '=', &Z, &Z };
    Tree CB  = { '>', &CC, &Z };
    Tree CA  = { '-', &CB, &Z };
    Tree C   = { '-', &D, &CA };
    Tree BB  = { '=', &Z, &Z };
    // Tree BA = {'*', &BB, &Z};
    // Tree B = {'*', &C, &BA};
    Tree B  = { '*', &C, &BB };
    Tree AB = { '=', &Z, &Z };
    Tree AA = { '+', &AB, &Z };
    Tree A  = { '+', &B, &AA };

    char * operator = malloc(sizeof(char) * 1);

    if (operator == NULL)
        return ERROR;

    operator[0] = '\0';
    char * tmp = malloc(strlen(line->lineValue) * sizeof(char));
    if (tmp == NULL)
        return ERROR;

    tmp = cpyString(line->lineValue);
    travelTree(&A, operator, &tmp, line->lineNumber, filename, &A);
    free(tmp);
    free(operator);
    return SUCCESS;
} /* doTmp */
