/**
 * @Author: Luis Valdez,Bastien Nisole,Patrick Zhuang
 * @Date:   2018-12-09T16:50:38+01:00
 * @Email:  patrickzhuang@hotmail.fr
 * @Project: Linter-c
 * @Filename: getIncludeFiles.c
 * @Last modified by:   Luis Valdez
 * @Last modified time: 2018-12-11T03:00:13+01:00
 */


#include "headers.h"

char ** getExcludeFiles(FILE * confFile, ArrayInfo * excludedFiles) {
    if (confFile == NULL) return NULL;

    char * fileName         = malloc(500);
    char excludedFilesFound = 0;

    fseek(confFile, 0, SEEK_SET);


    while (fgets(fileName, 500, confFile) != NULL) {
        trimString(fileName);

        if (!strcmp(fileName, EXCLUDEDFILES_HEADER)) {
            excludedFilesFound = 1;
        } else if (excludedFilesFound) {
            if (fileName[0] == '=')
                break;
            if (fileName[0] == 0)
                continue;
            if (!excludedFiles->size) {
                excludedFiles->fileToCheck = malloc(sizeof(char *));
            } else {
                excludedFiles->fileToCheck =
                  realloc(excludedFiles->fileToCheck, sizeof(char *) * (excludedFiles->size + 1) );
            }
            excludedFiles->fileToCheck[excludedFiles->size] = malloc(strlen(fileName));
            strcpy(excludedFiles->fileToCheck[excludedFiles->size], fileName + 1);
            excludedFiles->size += 1;
        }
    }

    fseek(confFile, 0, SEEK_SET);
    return NULL;
} /* getExcludeFiles */

// getIncludeFileHandler
int getIncludeFilesHandler(Linter * linter, ArrayInfo * excludedFiles) {
    linter->includedFiles->size = 0;
    linter->includedFiles       = recu(".", linter->recursive, linter->includedFiles, excludedFiles);
    return 1;
}

// Print string array
void printArray(char ** array, int size) {
    int i = 0;

    for (i = 0; i <= size; i++) {
        printf("Array [%d] %s\n", i, array[i]);
    }
}

// Free a 2d array
void free2dArray(char ** array, int size) {
    int i = 0;

    for (i = 0; i < size; i++) {
        free(array[i]);
    }
    free(array);
}

// Allocate a 2d array of 1 row
void alloc(char *** array) {
    (*array) = (char **) malloc(sizeof(char *) * 1);
    if ((*array) == NULL)
        return;
}

// Reallocate a 2D array
void realloc2DArray(char *** ptr, int size) {
    (*ptr) = (char **) realloc((*ptr), (size + 1) * sizeof(*ptr));
    if (ptr == NULL)
        return;
}

// Check is the file is banned
int isOk(char * str, ArrayInfo * excludedFiles) {
    int i = 0;

    for (i = 0; i < excludedFiles->size; i++) {
        if (strcmp(str, excludedFiles->fileToCheck[i]) == 0) {
            return 0;
        }
    }
    return 1;
}

// Return file to read (Bastien Nisole)
ArrayInfo * recu(char * str, char recursif, ArrayInfo * fileChecked, ArrayInfo * excludedFiles) {
    DIR * file = opendir(str);

    if (file == NULL)
        return NULL;

    struct dirent * dp;
    while ((dp = readdir(file)) != NULL) {
        // find the last occurence of . in the name
        char * dot = strrchr(dp->d_name, '.');
        DIR * drtmp;
        char * tmp = malloc(sizeof(char) * (strlen(str) + dp->d_namlen + 2));
        if (tmp == NULL)
            return NULL;

        sprintf(tmp, "%s\\%s", str, dp->d_name);

        if ((drtmp = opendir(tmp)) != NULL &&
          strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && recursif)
        {
            recu(tmp, recursif, fileChecked, excludedFiles);
            closedir(drtmp);
            // Si la dot existe ( son pointeur != de NULL  & que dot est �gale a ".c"
            // Si ( 1 && !(0) )
            // Si ( 1 && 1 )
        } else if (dot && !strcmp(dot, ".c") && isOk(dp->d_name, excludedFiles)) {
            if (fileChecked->size == 0) {
                alloc(&fileChecked->fileToCheck);
            } else {
                realloc2DArray(&fileChecked->fileToCheck, fileChecked->size);
            }
            fileChecked->fileToCheck[fileChecked->size] = malloc(sizeof(char) * (strlen(dp->d_name) + strlen(str) + 3));
            if (fileChecked->fileToCheck[fileChecked->size] == NULL) {
                return NULL;
            }
            sprintf(fileChecked->fileToCheck[fileChecked->size], "%s\\%s", str, dp->d_name);
            fileChecked->size++;
        }
        free(tmp);
    }
    closedir(file);
    return fileChecked;
} /* recu */
