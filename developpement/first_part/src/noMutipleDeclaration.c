/**
 * @Author: Luis Valdez,Bastien Nisole,Patrick Zhuang
 * @Date:   2018-12-09T16:50:38+01:00
 * @Email:  patrickzhuang@hotmail.fr
 * @Project: Linter-c
 * @Filename: noMutipleDeclaration.c
 * @Last modified by:   Luis Valdez
 * @Last modified time: 2018-12-11T03:00:28+01:00
 */


#include "headers.h"


/**
 *  check multiple declaration on same line
 *
 * @method checkMultipleDeclaration
 *
 * @param  line          current line
 *         filename      current file name
 *
 * @return               return 1 if success
 */
int checkMultipleDeclaration(struct Line * line, char * filename) {
    char * type [9] = { "float", "int", "long", "char", "unsigned", "FILE", "short", "double", "void" };
    char word [500];
    char * stringToTest;
    int i = 0;
    int y = 0;
    int fBracket;
    int lBracket;
    int fParenthesis;
    int lParenthesis;
    char isRespected = 1;

    while (line->lineValue[i] == ' ') i++;
    while (line->lineValue[i] != ' ' && i < strlen(line->lineValue)) {
        word[y] = line->lineValue[i];
        i++;
        y++;
    }
    word[y]      = '\0';
    stringToTest = nothingInBracketOrParenthesis(line->lineValue);
    for (i = 0; i < 9; i++) {
        if (strcmp(word, type[i]) == 0 && strchr(stringToTest, ';') != NULL) {
            fBracket = strchr(stringToTest, '{') - stringToTest;
            lBracket = strrchr(stringToTest, '}') - stringToTest;
            if ( ( strchr(stringToTest,
              ',') - stringToTest) > lBracket ||
              ( strchr(stringToTest, ',') - stringToTest) < fBracket || (lBracket > fBracket))
            {
                fParenthesis = strchr(stringToTest, '{') - stringToTest;
                lParenthesis = strrchr(stringToTest, '}') - stringToTest;
                if ( ( strchr(stringToTest,
                  ',') - stringToTest) > lParenthesis ||
                  ( strchr(stringToTest, ',') - stringToTest) < fParenthesis || (lParenthesis > fParenthesis) )
                {
                    isRespected = 0;
                }
            }
            break;
        }
    }
    free(stringToTest);
    return isRespected ? RULE_RESPECTED : RULE_NOT_RESPECTED;
} /* checkMultipleDeclaration */

/**
 *  clear text between parenthesis and bracket
 *
 * @method nothingInBracketOrParenthesis
 *
 * @param  string        current string to test
 *
 * @return               return the string without bracket if success or NULL if malloc error
 */
char * nothingInBracketOrParenthesis(char * string) {
    char * stringToReturn = malloc(strlen(string) + 1);
    int i                = 0;
    int y                = 0;
    int bracket          = 0;
    int countBracket     = 0;
    int parenthesis      = 0;
    int countParenthesis = 0;

    if (stringToReturn == NULL) {
        return NULL;
    } else {
        while (i < strlen(string)) {
            if (string[i] == '{' && bracket == 0) {
                bracket = 1;
                countBracket++;
            } else if (string[i] == '(' && parenthesis == 0) {
                parenthesis = 1;
                countParenthesis++;
            } else if (string[i] == '}' && bracket == 1) {
                i++;
                countBracket--;
                if (countBracket == 0)
                    bracket = 0;
            } else if (string[i] == ')' && parenthesis == 1) {
                i++;
                countParenthesis--;
                if (countParenthesis == 0)
                    parenthesis = 0;
            }
            if (bracket == 0 && parenthesis == 0) {
                stringToReturn[y] = string[i];
                y++;
            }
            i++;
        }
        if (countParenthesis != 0 || countBracket != 0) {
            return string;
        }
        stringToReturn[y] = '\0';
        return stringToReturn;
    }
} /* nothingInBracketOrParenthesis */
