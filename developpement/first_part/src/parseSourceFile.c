/**
 * @Author: Luis Valdez
 * @Date:   2018-12-09T13:36:49+01:00
 * @Email:  luis@valdez.fr
 * @Project: Linter-c
 * @Filename: parseSourceFile.c
 * @Last modified by:   Luis Valdez
 * @Last modified time: 2018-12-11T03:00:31+01:00
 */


#include "../include/headers.h"

char prototypeExist(Functions * funcs, FILE * f, char * filename) {
    int i;
    char * str = NULL, * proto = NULL, notFound = 1, * newString;
    char error[500];

    for (i = 0; i < funcs->size; i++) {
        fseek(f, 0, SEEK_SET);
        str = malloc(funcs->funcs[i]->endPosition - funcs->funcs[i]->initPosition + 1);
        if (str == NULL) return ERROR;

        // fgets(str, funcs->funcs[i]->endPosition - funcs->funcs[i]->initPosition, f);
        fread(str, sizeof(char), funcs->funcs[i]->endPosition - funcs->funcs[i]->initPosition, f);
        str[funcs->funcs[i]->endPosition - funcs->funcs[i]->initPosition] = 0;

        if (!(proto = strstr(str, funcs->funcs[i]->name))) {
            sprintf(error, "[%s] %s n'a pas de prototype\n", filename, funcs->funcs[i]->name);
            writeErrorFile(error);
        } else {
            while ((newString = checkProto(str, proto, funcs->funcs[i])) != NULL) {
                if ((proto = strstr(newString, funcs->funcs[i]->name)) == NULL) {
                    notFound = 0;
                    break;
                }
            }
            if (notFound == 0) {
                sprintf(error, "[%s] %s n'a pas de prototype\n", filename, funcs->funcs[i]->name);
                writeErrorFile(error);
            }
        }
    }

    return SUCCESS;
} /* prototypeExist */

char * checkProto(char * str, char * pos, Function * func) {
    char * cpy = malloc(strlen(str) + 1);
    char * returnPtr, * type;
    char * namePos = pos;
    char * params, * tempParams;

    if (cpy == NULL) return NULL;

    cpy       = cpyString(str);
    returnPtr = startFunc(cpy, &pos);

    type = getTypeProto(pos, namePos);
    trimString(type);
    trimString(func->type);
    if (strcmp(func->type, type)) {
        return returnPtr;
    }

    params     = getProtoParams(pos);
    params     = parseParams(params);
    tempParams = parseParams(func->paramsString);

    trimString(params);
    trimString(tempParams);

    if (!strcmp(params, tempParams)) {
        return NULL;
    }

    free(cpy);
    free(type);

    return returnPtr;
} /* checkProto */

char * getTypeProto(char * str, char * namePtr) {
    int count = 0, i;
    char * type = NULL;

    count = namePtr - str;
    if (count <= 0) return NULL;

    type = malloc((count + 1) * sizeof(char));
    if (type == NULL) return NULL;

    for (i = 0; i < count; i++) {
        type[i] = str[i];
    }
    type[i] = 0;

    return type;
}

char * getProtoParams(char * proto) {
    char * params    = strstr(proto, "(");
    char * endParams = strstr(proto, ")");

    if (params == NULL || endParams == NULL) return NULL;

    *(endParams + 1) = 0;

    return params;
}

char * startFunc(char * str, char ** pos) {
    char * end;

    (*pos)--;
    while ((*(*pos) == ' ' || *(*pos) == '\n' || *(*pos) == '*') && *pos != str) (*pos)--;
    while (*(*pos) != '\n' && *(*pos) != ';' && *pos != str) (*pos)--;
    (*pos)++;

    end = *pos;
    while (*end != ';' && end < (str + strlen(str))) end++;

    *end = 0;

    return end + 1;
}

Function * getFunction(FILE * source) {
    if (source == NULL) return NULL;

    char error[500];
    Function * func = newFunc();
    if (func == NULL) return NULL;

    int initFunction = -1, endFunction = -1;
    int openFunctionBracket = 0, closeFunctionBracket = 0, line = 1;

    char currentChar = 0, previousChar = 0;
    while ((currentChar = getc(source)) != EOF) {
        if (currentChar == '{' && previousChar != '"' && previousChar != '\'') {
            if (openFunctionBracket == 0) {
                initFunction = ftell(source);
            }
            openFunctionBracket += 1;
        } else if (currentChar == '}' && previousChar != '"' && previousChar != '\'') {
            closeFunctionBracket += 1;
        } else if (currentChar == '\n') {
            line++;
        }

        if (openFunctionBracket && closeFunctionBracket && closeFunctionBracket == openFunctionBracket) {
            endFunction        = ftell(source);
            func->content      = getContentFunction(source, initFunction, endFunction);
            func->paramsString = getParams(source, &initFunction);
            func->name         = getFuncName(source, &initFunction);
            func->type         = getTypeFunc(source, &initFunction);
            func->initPosition = initFunction;
            func->endPosition  = endFunction;
            break;
        }

        previousChar = currentChar;
    }

    if (currentChar == EOF && closeFunctionBracket != openFunctionBracket && openFunctionBracket &&
      closeFunctionBracket)
    {
        sprintf(error, "file : s -> error bracket \n");
        writeErrorFile(error);
        return NULL;
    }

    if (currentChar == EOF) return NULL;

    fseek(source, endFunction, SEEK_SET);
    return func;
} /* getFunction */

char * getContentFunction(FILE * source, int start, int end) {
    char * content = malloc((end - start) * sizeof(char));
    int index      = 0;

    fseek(source, start, SEEK_SET);
    for (index = 0; index < end - start - 1; index++) {
        content[index] = getc(source);
    }
    // while (ftell(source) < end - 1) {
    //     content[index] = getc(source);
    //     index++;
    // }

    content[index] = 0;

    return content;
}

char * getParams(FILE * source, int  * position) {
    if (position == NULL || source == NULL) return NULL;

    char currentChar = 0, startParams = 0, * params = NULL;
    int size = 0, /*positionTemp,*/ i;
    fseek(source, *position, SEEK_SET);


    while (*position > 0) {
        *position -= 1;
        fseek(source, (*position), SEEK_SET);
        currentChar = getc(source);
        if (currentChar == ')') startParams = 1;
        else if (currentChar == '(') break;
        if (startParams) size++;
    }

    if (*position <= 0 || !size) return NULL;

    /*positionTemp = *position - 1;*/
    fseek(source, (*position), SEEK_SET);
    params = malloc(sizeof(char) * (++size + 1));
    if (params == NULL) return params;

    for (i = 0; i < size; i++) {
        params[i] = getc(source);
    }
    params[i] = 0;

    return params;
} /* getParams */

char * getFuncName(FILE * source, int * position) {
    char currentChar, * nameFunc = NULL;
    int size = 0, i = 0;

    while (*position > 1) {
        *position -= 1;
        fseek(source, (*position), SEEK_SET);
        currentChar = getc(source);
        if (currentChar != ' ' && currentChar != '\n') {
            *position += 1;
            fseek(source, (*position), SEEK_SET);
            break;
        }
    }

    if (*position == 1) return NULL;

    while (*position > 1) {
        *position -= 1;
        fseek(source, (*position), SEEK_SET);
        currentChar = getc(source);
        if (currentChar == ' ' || currentChar == '\n' || currentChar == '*') break;
        size++;
    }

    nameFunc = malloc((size + 1) * sizeof(char));
    if (nameFunc == NULL) return nameFunc;

    for (i = 0; i < size; i++) {
        nameFunc[i] = getc(source);
    }

    nameFunc[i] = 0;
    return nameFunc;
} /* getFuncName */

char * getTypeFunc(FILE * source, int  * position) {
    char currentChar, * typeFunc = NULL;
    int size = 0, i = 0;

    while (*position > 1) {
        fseek(source, (*position), SEEK_SET);
        *position  -= 1;
        currentChar = getc(source);
        if (currentChar != ' ' && currentChar != '\n') {
            *position += 2;
            fseek(source, (*position), SEEK_SET);
            break;
        }
    }

    if (*position == 1) return NULL;

    while (*position > 1) {
        *position -= 1;
        fseek(source, *position, SEEK_SET);
        currentChar = getc(source);
        if (currentChar == '\n' || currentChar == ';' || currentChar == '}') break;
        size++;
    }

    typeFunc = malloc((size + 1) * sizeof(char));
    if (typeFunc == NULL) return typeFunc;

    for (i = 0; i < size; i++) {
        typeFunc[i] = getc(source);
    }

    typeFunc[i] = 0;
    return typeFunc;
} /* getTypeFunc */

FILE * cpyNofunc(FILE * source, Functions * arrayFunc) {
    fseek(source, 0, SEEK_SET);

    FILE * file = fopen("temp.c", "wb");
    char chara;
    int pos = ftell(source);
    while ((chara = fgetc(source)) != EOF) {
        if (WriteOrNot(pos, arrayFunc) || chara == '\n') {
            fputc(chara, file);
        } else {
            fputc('`', file);
        }
        pos = ftell(source);
    }

    fclose(file);
    file = fopen("temp.c", "rb");
    return file;
}

char WriteOrNot(int pos, Functions * arrayFunc) {
    int i;

    for (i = 0; i < arrayFunc->size; i++) {
        if (pos > arrayFunc->funcs[i]->initPosition && pos < arrayFunc->funcs[i]->endPosition) {
            return FALSE;
        }
    }

    return TRUE;
}

char * parseParams(char * params) {
    if (params == NULL || !strlen(params)) return NULL;

    char * temp      = params;
    char * nextParam = params;

    while (*temp != 0) {
        if (*temp == '`') {
            *temp = ' ';
        }
        temp++;
    }

    do {
        temp = nextParam;

        while (*temp != ',' && *temp != ')') temp++;
        if (*temp == ')') nextParam = NULL;
        else nextParam = temp + 1;
        temp--;
        while (*temp == ' ') temp--;
        if (*temp == '(') return params;

        while (*temp != ' ' && *temp != '*') {
            *temp = ' ';
            temp--;
        }
    } while (nextParam != NULL);

    return params;
} /* parseParams */

int parseFuncFiles(Linter * linter) {
    int i;
    FILE * file;

    for (i = 0; i < linter->includedFiles->size; i++) {
        file = fopen(linter->includedFiles->fileToCheck[i], "rb");
        if (file != NULL) {
            parseFunctions(file, linter->includedFiles->fileToCheck[i]);
            if (file != NULL) fclose(file);
        } else {
            return ERROR;
        }
    }

    return SUCCESS;
}

int parseFunctions(FILE * f, char * filename) {
    FILE * cpy, * noComment;

    Functions * funcs = newArrayFunc();
    Function * func   = NULL;

    noCommentFile(f);
    noComment = fopen("tempNoComment.c", "rb");

    while ((func = getFunction(f)) != NULL) {
        if (funcs->funcs == NULL) {
            funcs->funcs    = malloc(sizeof(Function *));
            funcs->funcs[0] = cpyFunc(func);
            funcs->size++;
        } else {
            addFunc(funcs, func);
        }
    }


    cpy = cpyNofunc(noComment, funcs);

    fclose(noComment);

    prototypeExist(funcs, cpy, filename);

    fclose(cpy);
    remove("temp.c");
    remove("tempNoComment.c");
    destroyArrayFunc(funcs);

    return 1;
} /* parseFunctions */
