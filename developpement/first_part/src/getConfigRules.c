/**
 * @Author: Luis Valdez,Bastien Nisole,Patrick Zhuang
 * @Date:   2018-12-04T22:12:59+01:00
 * @Email:  luis@valdez.fr
 * @Project: Linter-c
 * @Filename: mainEventHandler.c
 * @Last modified by:   Luis Valdez
 * @Last modified time: 2018-12-11T03:00:10+01:00
 */


#include "headers.h"

Linter * getConfig() {
    char confDirectoryName[200];
    char filename[200];
    Linter * linter = newLinter();

    ArrayInfo * excludedFiles = malloc(sizeof(ArrayInfo));

    if (excludedFiles == NULL) return NULL;

    excludedFiles->size = 0;

    printf("Saisissez le chemin du dossier qui contient les fichiers de configuration :");
    fgets(confDirectoryName, 199, stdin);
    confDirectoryName[strlen(confDirectoryName) - 1] = '\0';
    printf("\nSaisissez le nom du fichier de configuration principal :");
    fgets(filename, 199, stdin);
    filename[strlen(filename) - 1] = '\0';
    if (recursiveSearchExtends(confDirectoryName, filename, linter, excludedFiles) == ERROR) {
        return NULL;
    }

    getIncludeFilesHandler(linter, excludedFiles);
    return linter;
} /* eventHandler */

int recursiveSearchExtends(char * confDirectoryName, char * filename, Linter * linter, ArrayInfo * excludedFiles) {
    char dumpString[200];
    char fullFileName[200];
    char toBeCheckedFileName[200];
    FILE * fileToBeChecked;

    strcpy(fullFileName, confDirectoryName);
    strcat(fullFileName, filename);
    fullFileName[strlen(fullFileName)] = '\0';
    FILE * file = fopen(fullFileName, "r");

    if (file == NULL) {
        printf("File doesn't exist (recursiveSearchExtends) : %s%s\n", confDirectoryName, filename);
        return ERROR;
    } else {
        if (checkIntegrity(file)) {
            fgets(dumpString, 199, file);
            while (strstr(dumpString, "extends") == NULL && !feof(file)) {
                fgets(dumpString, 199, file);
            }
            if (!feof(file)) {
                fgets(dumpString, 199, file);
                dumpString[strlen(dumpString) - 1] = '\0';
                fclose(file);
                if (recursiveSearchExtends(confDirectoryName, dumpString, linter, excludedFiles) == ERROR) {
                    return ERROR;
                }
            }
            // Ici est la nom du fichier du dernier extends
            strcpy(toBeCheckedFileName, confDirectoryName);
            strcat(toBeCheckedFileName, filename);
            toBeCheckedFileName[strlen(toBeCheckedFileName)] = '\0';
            fileToBeChecked = fopen(toBeCheckedFileName, "r");
            if (fileToBeChecked == NULL) {
                return ERROR;
            }
            // printArray(excludedFiles->fileToCheck,excludedFiles->size -1 );
            getExcludeFiles(fileToBeChecked, excludedFiles);
            getRecursiveConfig(fileToBeChecked, linter);
            getRules(fileToBeChecked, linter);

            fclose(fileToBeChecked);
            return SUCCESS;
        } else {
            printf("Source de l'erreur : %s\n", fullFileName);
            return ERROR;
        }
    }
} /* recursiveSearchExtends */
