/**
 * @Author: Luis Valdez,Bastien Nisole,Patrick Zhuang
 * @Date:   2018-11-28T00:51:45+01:00
 * @Email:  patrickzhuang@hotmail.fr
 * @Project: Linter-c
 * @Filename: checkLine.c
 * @Last modified by:   Luis Valdez
 * @Last modified time: 2018-12-11T03:00:02+01:00
 */


#include "headers.h"

/**
 * Browse all files that have to be checked
 * @param  linter Linter struct with all conf datas
 * @return        ERROR when can't open file/SUCCES if done
 */
int checkWrittingRules(Linter * linter) {
    int i;
    FILE * file;

    for (i = 0; i < linter->includedFiles->size; i++) {
        file = fopen(linter->includedFiles->fileToCheck[i], "rb");
        if (file != NULL) {
            browseFile(file, linter->includedFiles->fileToCheck[i], linter);
            fclose(file);
        } else {
            return ERROR;
        }
    }

    return SUCCESS;
}

// Function which browse each lines of a file.
int browseFile(FILE * file, char *  filename, Linter * linter) {
    int count = 1, currentIndent = 0, diffParenthesis = -9999;
    Line * tempLine = NULL;
    char lineType   = EMPTY_STRING;
    char tempString[500];
    char hasHeader = IN_CHECK;

    while ((tempLine = getASingleLine(file, count, &lineType)) != NULL) {
        if (tempLine == NULL) {
            printf("\n End of file \n");
            break;
        }

        checkASingleLine(linter, tempLine, filename, &hasHeader, lineType, &currentIndent, &diffParenthesis);
        // printf("%d/%d)%s",count,tempLine->lineValue[0],tempLine->lineValue);
        count += 1;
        free(tempLine->lineValue);
        free(tempLine);
    }

    if (linter->rules->maxFileLineNumbers != -1 &&
      checkFileMax(linter->rules->maxFileLineNumbers, count) == RULE_NOT_RESPECTED)
    {
        sprintf(tempString, "[%s] Ce fichier depasse le nombre de maximal de lignes autorisees : %d \n", filename,
          linter->rules->maxFileLineNumbers);
        writeErrorFile(tempString);
    }

    return SUCCESS;
} /* browseFile */

/**
 * Function which calls other functions which will check writting rules
 * @param  linter          Linter config structure
 * @param  line            Line strucuture where the line, the line number and the indent value will be saved
 * @param  filename        Name of the file in check
 * @param  hasHeader       Save the current status of header
 * @param  lineType        Status of the line (EMPTY_STRING/NORMAL/IN_COMMENT)
 * @param  currentIndent   indent value of the line
 * @param  diffParenthesis
 * @return                 [description]
 */
int checkASingleLine(Linter * linter, Line * line, char * filename, char * hasHeader, char lineType,
  int * currentIndent, int * diffParenthesis) {
    char tempString[500];

    if (linter->rules->indent != -1 && !checkIndent(line, currentIndent, linter->rules->indent, diffParenthesis)) {
        sprintf(tempString, "[%s : %d] L'indentation n'est pas respectee.\n", filename, line->lineNumber);
        writeErrorFile(tempString);
    }
    if (linter->rules->arrayBracketEol != -1 && !checkBracket(line)) {
        sprintf(tempString, "[%s : %d] L'accolade est mal placee.\n", filename, line->lineNumber);
        writeErrorFile(tempString);
    }

    if (linter->rules->commentsHeader != -1 && (*hasHeader) != CHECKED &&
      ( ( (*hasHeader) = checkHeader(lineType, (*hasHeader) ) ) == NO_HEADER ) )
    {
        sprintf(tempString, "[%s] Ce fichier n'a pas d'header.\n", filename);
        writeErrorFile(tempString);
    }
    if (linter->rules->commaSpacing != -1) {
        checkCommaSpacing(line, filename);
    }

    if (linter->rules->operatorsSpacing != -1) {
        doTmp(line, filename);
    }
    if (linter->rules->noMultiDeclaration != -1 && checkMultipleDeclaration(line, filename) == RULE_NOT_RESPECTED) {
        sprintf(tempString, "[%s : %d] Declaration multiple.\n", filename, line->lineNumber);
        writeErrorFile(tempString);
    }
    if (linter->rules->maxLineNumbers != -1 &&
      checkLineMax(linter->rules->maxLineNumbers, line) == RULE_NOT_RESPECTED)
    {
        sprintf(tempString, "[%s : %d] Maximum de caractere par ligne (%d) depasse.\n", filename, line->lineNumber,
          linter->rules->maxLineNumbers);
        writeErrorFile(tempString);
    }
    if (linter->rules->noTrailingSpaces != -1 && checkNoTrailingSpaces(line) == RULE_NOT_RESPECTED) {
        sprintf(tempString, "[%s : %d] Espace en fin de ligne.\n", filename, line->lineNumber);
        writeErrorFile(tempString);
    }

    return SUCCESS;
} /* checkASingleLine */

int checkFileMax(int maxFileLineNumbers, int count) {
    if (maxFileLineNumbers < count) {
        return RULE_NOT_RESPECTED;
    }
    return RULE_NOT_RESPECTED;
}

int checkLineMax(int maxLineNumbers, Line * line) {
    if (maxLineNumbers < strlen(line->lineValue)) {
        return RULE_NOT_RESPECTED;
    }
    return RULE_RESPECTED;
}

int checkNoTrailingSpaces(Line * line) {
    int i;
    int countSpaces = 0;

    for (i = strlen(line->lineValue) - 2; i >= 0; i--) {
        if (line->lineValue[i] == ' ') {
            countSpaces += 1;
        } else if (line->lineValue[i] == '\n' || line->lineValue[i] == '\r') {
            continue;
        } else {
            if (countSpaces) {
                return RULE_NOT_RESPECTED;
            } else {
                return RULE_RESPECTED;
            }
        }
    }

    return RULE_RESPECTED;
}

char checkHeader(char lineType, char hasHeader) {
    switch (hasHeader) {
        case NO_HEADER:
        case HAS_HEADER:
            return CHECKED;

        case IN_CHECK:
            if (lineType == IN_COMMENT) {
                return HAS_HEADER;
            } else if (lineType == NORMAL) {
                return NO_HEADER;
            } else if (lineType == EMPTY_STRING) {
                return IN_CHECK;
            }
            break;

        default:
            return HEADER_ERROR;

            break;
    }
    return HEADER_ERROR;
}
