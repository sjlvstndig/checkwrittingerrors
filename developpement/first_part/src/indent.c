/**
 * @Author: Luis Valdez,Bastien Nisole,Patrick Zhuang
 * @Date:   2018-11-19T21:56:14+01:00
 * @Email:  luis@valdez.fr
 * @Project: Linter-c
 * @Filename: indent.c
 * @Last modified by:   Luis Valdez
 * @Last modified time: 2018-12-11T03:00:16+01:00
 */


#include "headers.h"

/**
 * Main function for check indent
 *
 * @method checkIndent
 *
 * @param  line          the line where we check indent
 * @param  currentIndent currentIndent value
 * @param  increment     value of increment of indent
 *
 * @return               1 if indent is correct and else 0
 */
int checkIndent(Line * line, int * currentIndent, int increment, int * diffParenthesis) {
    char res, * lineCopy;
    int index = 0, bracketOpened = 0, brackClosed = 0;
    int parenthesisOpened = 0, parenthesisClosed = 0, diffTemp;

    if (currentIndent == NULL || line == NULL || line->lineValue == NULL) return 0;


    if (line->lineValue[0] == 13 || line->lineValue[0] == '\n' || line->lineValue[0] == 10 ||
      line->lineValue[0] == '\r')
    {
        return 1;
    }

    lineCopy = cpyString(line->lineValue);
    trimString(lineCopy);

    if (!*diffParenthesis) {
        if (lineCopy[0] == '{') {
            *diffParenthesis = -9999;
        } else {
            *currentIndent = *currentIndent + increment;
        }
    }

    if (!initParenthesisCount(*diffParenthesis, &parenthesisOpened, &parenthesisClosed)) return 0;


    if (line->indentValue != *currentIndent) res = 0;
    else res = 1;

    while (line->lineValue[index] != '\0') {
        if (line->lineValue[index] == '{') {
            bracketOpened++;
            if (parenthesisClosed == parenthesisOpened) {
                parenthesisOpened = 0;
                parenthesisClosed = 0;
            }
        } else if (line->lineValue[index] == '}') {
            brackClosed++;
        } else if (line->lineValue[index] == '(') {
            parenthesisOpened++;
        } else if (line->lineValue[index] == ')') {
            parenthesisClosed++;
        } else if (*diffParenthesis && parenthesisOpened == parenthesisClosed && line->lineValue[index] == ';') {
            parenthesisOpened = 0;
            parenthesisClosed = 0;
        } else if (!*diffParenthesis && line->lineValue[index] == ';') {
            *currentIndent    = *currentIndent - increment;
            *diffParenthesis  = -9999;
            parenthesisOpened = 0;
            parenthesisClosed = 0;
        }
        index++;
    }

    diffTemp = *diffParenthesis;
    if (!parenthesisOpened && !parenthesisClosed) {
        *diffParenthesis = -9999;
    } else {
        *diffParenthesis = parenthesisClosed - parenthesisOpened;
    }
    res = checkClosedBracketAlone(line, res, *currentIndent, increment);

    if (diffTemp != -9999 && diffTemp < 0) res = 1;

    *currentIndent = indentIncrement(*currentIndent, increment, brackClosed - bracketOpened);


    return res;
} /* checkIndent */

/**
 * change the current value of indent increment
 *
 * @method indentIncrement
 *
 * @param  currentValue    current value of indent increment
 * @param  increment       value of increment
 * @param  diff            the difference between closed bracket and opened bracket
 *
 * @return                 return the new value of increment
 */
int indentIncrement(int currentValue, int increment, int diff) {
    if (diff > 0) {
        currentValue -= (increment * diff);
    } else if (diff < 0) {
        currentValue += (increment * (diff * -1));
    }

    return currentValue;
}

/**
 * check if the line contains only closed brackets
 *
 * @method checkClosedBracketAlone
 *
 * @param  string                  the Line to check
 * @param  res                     the current result of indent check
 * @param  currentIndent           the current indent
 *
 * @return                         the new value of the result of indent check
 */
char checkClosedBracketAlone(Line * string, int res, int currentIndent, int increment) {
    char * temp = malloc(sizeof(char) * (strlen(string->lineValue) + 1));

    if (temp == NULL) return 0;

    strcpy(temp, string->lineValue);
    trimString(temp);

    if (strlen(temp) > 0 && temp[0] == '}') {
        // printf("real indent : %d\n", currentIndent - increment);
        if (string->indentValue != (currentIndent - increment)) res = 0;
        else res = 1;
    }

    free(temp);

    return res;
}

/**
 * init count parenthesis
 *
 * @method initParenthesisCount
 *
 * @param  diffParenthesis      diff
 * @param  parenthesisOpened    count
 * @param  parenthesisClosed    count
 *
 * @return                      1 success or 0 error
 */
int initParenthesisCount(int diffParenthesis, int * parenthesisOpened, int * parenthesisClosed) {
    if (parenthesisClosed == NULL || parenthesisOpened == NULL) return 0;

    if (diffParenthesis == -9999 || !diffParenthesis) return 1;

    if (diffParenthesis > 0) return 0;

    *parenthesisOpened = diffParenthesis * -1;
    *parenthesisClosed = 0;

    return 1;
}
