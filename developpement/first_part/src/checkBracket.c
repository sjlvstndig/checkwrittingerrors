/**
 * @Author: Luis Valdez,Bastien Nisole,Patrick Zhuang
 * @Date:   2018-11-21T23:13:05+01:00
 * @Email:  luis@valdez.fr
 * @Project: Linter-c
 * @Filename: checkBracket.c
 * @Last modified by:   Luis Valdez
 * @Last modified time: 2018-12-11T03:00:00+01:00
 */

#include "headers.h"

int checkBracket(Line * line) {
    if (line == NULL || line->lineValue == NULL) return ERROR;

    char * temp = cpyString(line->lineValue), * foundBracket;
    if (temp == NULL) return ERROR;

    char res;

    trimString(temp);

    foundBracket = strstr(temp, "{");

    if (foundBracket == NULL) {
        res = SUCCESS;
    } else {
        if (foundBracket - temp > 0) res = SUCCESS;
        else res = ERROR;
    }


    free(temp);

    return res;
}
