/**
 * @Author: Luis Valdez,Bastien Nisole,Patrick Zhuang
 * @Date:   2018-12-09T19:18:30+01:00
 * @Email:  patrickzhuang@hotmail.fr
 * @Project: Linter-c
 * @Filename: utils.c
 * @Last modified by:   Luis Valdez
 * @Last modified time: 2018-12-11T03:00:37+01:00
 */


#include "headers.h"


/**
 * remove all white spaces of a string
 *
 * @method trimString
 *
 * @param  s          the string to trim
 */
void trimString(char * s) {
    if (s == NULL) return;

    char * i = s;
    char * j = s;
    do {
        *i = *j;
        if (*i != ' ' && *i != '\n')
            i++;
    } while (*j++ != 0);
}

/**
 * copy a string
 *
 * @method cpyString
 *
 * @param  s         the string that you want to copy
 *
 * @return           the copy
 */
char * cpyString(char * s) {
    if (s == NULL) return s;

    char * temp = malloc(sizeof(char) * (strlen(s) + 1));
    if (temp == NULL) return temp;

    strcpy(temp, s);

    return temp;
}

/**
 * Function which will get a single line of a file whithout comment and emptied string within quotes
 * @param  file       file in which the line will be read
 * @param  lineNumber line number
 * @param  lineType   current line type (NORMAL/EMPTY_STRING/IN_COMMENT)
 * @return            return a Line structure if success else error if failed or end of file
 */
Line * getASingleLine(FILE * file, int lineNumber, char * lineType) {
    int size = 0, indentEnd = 0, indentValue = 0, hasInit = FALSE, hasChanged = TRUE;
    Line * returnedLine = malloc(sizeof(Line) * 1);
    char currentChar    = 'a';

    if (returnedLine == NULL) {
        printf("Error malloc returnedLine in getASingleLine");
        return NULL;
    }
    if (feof(file)) {
        return NULL;
    }
    returnedLine->lineNumber = lineNumber;
    // Check if first char of line is a begin comment ou end comment
    currentChar = fgetc(file);
    if (!feof(file)) {
        if ( ( (*lineType) == NORMAL || (*lineType) == EMPTY_STRING ) ) {
            checkBeginComment(currentChar, file, lineType);
            if ( (*lineType) == NORMAL || (*lineType) == EMPTY_STRING) {
                hasChanged = FALSE;
            }
        } else if ( (*lineType) == IN_COMMENT) {
            checkEndComment(currentChar, file, lineType);
            if ( (*lineType) == IN_COMMENT) {
                hasChanged = FALSE;
            }
        }
    }

    if (hasChanged == FALSE) {
        fseek(file, -1, SEEK_CUR);
    }
    // Get line's first char if isn't comment
    if (( (*lineType) == NORMAL || (*lineType) == EMPTY_STRING )  ) {
        returnedLine->lineValue = initLine(file);
        hasInit     = TRUE;
        currentChar = returnedLine->lineValue[0];
        *lineType   = NORMAL;
        if (returnedLine->lineValue[0] == ' ') {
            indentValue++;
        } else {
            indentEnd = 1;
        }
    }

    while (currentChar != '\n' && !feof(file)) {
        currentChar = fgetc(file);
        hasChanged  = TRUE;
        if ( (*lineType) == NORMAL || (*lineType) == EMPTY_STRING) {
            checkBeginComment(currentChar, file, lineType);
            if (*lineType == IN_COMMENT) {
                continue;
            } else {
                fseek(file, -1, SEEK_CUR);
            }
        } else if ( (*lineType) == IN_COMMENT) {
            checkEndComment(currentChar, file, lineType);
            if ((*lineType) == NORMAL || (*lineType) == EMPTY_STRING) {
                continue;
            }
        }
        if (*lineType != IN_COMMENT) {
            if (hasInit == TRUE) {
                currentChar = fgetc(file);
                size        = strlen(returnedLine->lineValue);
                returnedLine->lineValue           = realloc(returnedLine->lineValue, size + 2);
                returnedLine->lineValue[size]     = currentChar;
                returnedLine->lineValue[size + 1] = '\0';
                if (returnedLine->lineValue[0] == '\n' && returnedLine->lineValue[1] == '\0') {
                    *lineType = EMPTY_STRING;
                }
            } else {
                returnedLine->lineValue = initLine(file);
                hasInit     = TRUE;
                currentChar = returnedLine->lineValue[0];
                *lineType   = NORMAL;
            }
        }
        if (!indentEnd && currentChar == ' ') {
            indentValue++;
        } else {
            indentEnd = 1;
        }
    }
    if (hasInit == FALSE) {
        *lineType = IN_COMMENT;
        returnedLine->lineValue    = malloc(sizeof(char) * 2);
        returnedLine->lineValue[0] = '\n';
        returnedLine->lineValue[1] = '\0';
    } else {
        removeHardString(returnedLine->lineValue);
        checkInLineComment(returnedLine);
    }
    returnedLine->indentValue = indentValue;
    return returnedLine;
} /* getASingleLine */

void checkBeginComment(char currentchar, FILE * file, char * lineType) {
    // printf("\nhere : %c\n",currentchar);
    long currentPos = ftell(file);
    char nextChar   = fgetc(file);

    if (nextChar == '*' && currentchar == '/') {
        *lineType = IN_COMMENT;
        return;
    }
    fseek(file, currentPos, SEEK_SET);
}

void checkEndComment(char currentchar, FILE * file, char * lineType) {
    long currentPos = ftell(file);
    char nextChar   = fgetc(file);

    if (nextChar == '/' && currentchar == '*') {
        *lineType = NORMAL;
        return;
    }
    fseek(file, currentPos, SEEK_SET);
}

char * initLine(FILE * file) {
    char * returnString = malloc(sizeof(char) * 2);

    if (returnString == NULL) {
        return NULL;
    }
    returnString[0] = fgetc(file);
    returnString[1] = '\0';
    return returnString;
}

void checkInLineComment(Line * returnedLine) {
    char * tempPos = strstr(returnedLine->lineValue, "//");

    if (tempPos != NULL) {
        tempPos[0] = '\n';
        tempPos[1] = '\0';
    }
}

void removeHardString(char * s) {
    if (s == NULL) return;

    char * i = s;
    char * j = s;
    char quoteFound = 0, doubleQuoteFound = 0, escapeChar = 0, modified = 0;
    do {
        *i = *j;

        if (*i == '"' && !quoteFound && !doubleQuoteFound && !escapeChar) {
            doubleQuoteFound = 1;
            i++;
        } else if (*i == '\'' && !quoteFound && !doubleQuoteFound && !escapeChar) {
            quoteFound = 1;
            i++;
        } else if (doubleQuoteFound && *i == '"' && !escapeChar) {
            doubleQuoteFound = 0;
        } else if (quoteFound && *i == '\'' && !escapeChar) {
            quoteFound = 0;
        } else if ((doubleQuoteFound || quoteFound) && *i == '\\' && !escapeChar) {
            escapeChar = 1;
            modified   = 1;
        }

        if (!modified && escapeChar) escapeChar = 0;
        modified = 0;
        if (!quoteFound && !doubleQuoteFound) i++;
    } while (*j++ != 0);

    if (quoteFound || doubleQuoteFound) {
        printf("Error quote\n");
    }
}/* removeHardString */

void writeInFile(char * filename, char * outputString) {
    FILE * file = fopen(filename, "a+b");

    if (file == NULL) printf("File can't open in writeInFile");
    fprintf(file, "%s", outputString);
    fclose(file);
}

void writeErrorFile(char * outputString) {
    writeInFile("integrityError.txt", outputString);
}

void printErrors() {
    FILE * file = fopen("integrityError.txt", "rb");
    char tempString[500];

    while (!feof(file) ) {
        fgets(tempString, 499, file);
        printf("%s", tempString);
    }
    fclose(file);
}

void printCurrentRules(Linter * linter) {
    printf("*****************Debut print test objet linter *********************\n");
    printf("arrayBracketEol : %d\n", linter->rules->arrayBracketEol);
    printf("operatorsSpacing : %d\n", linter->rules->operatorsSpacing);
    printf("commaSpacing : %d\n", linter->rules->commaSpacing);
    printf("indent : %d\n", linter->rules->indent);
    printf("commentsHeader : %d\n", linter->rules->commentsHeader);
    printf("maxLineNumbers : %d\n", linter->rules->maxLineNumbers);
    printf("maxFileLineNumbers : %d\n", linter->rules->maxFileLineNumbers);
    printf("noTrailingSpaces : %d\n", linter->rules->noTrailingSpaces);
    printf("noMultiDeclaration : %d\n", linter->rules->noMultiDeclaration);
    printf("unusedVariable : %d\n", linter->rules->unusedVariable);
    printf("undeclaredVariable : %d\n", linter->rules->undeclaredVariable);
    printf("noPrototype : %d\n", linter->rules->noPrototype);
    printf("unusedFunction : %d\n", linter->rules->unusedFunction);
    printf("undeclaredFunction : %d\n", linter->rules->undeclaredFunction);
    printf("variableAssignmentType : %d\n", linter->rules->variableAssignmentType);
    printf("functionParametersType : %d\n", linter->rules->functionParametersType);
    printf("*****************Fin print test objet linter *********************\n");
}

void noCommentFile(FILE * file) {
    FILE * tmpFile = fopen("tempNoComment.c", "wb");
    long fileSize;
    char * tempString, tempChara;
    long i = 0, count;

    fseek(file, 0, SEEK_END);
    fileSize = ftell(file);
    fseek(file, 0, SEEK_SET);

    tempString = malloc(sizeof(char) * fileSize);
    if (tempString == NULL) {
        return;
    }

    while ((tempChara = fgetc(file)) != EOF) {
        tempString[i] = tempChara;
        i++;
    }

    count      = i;
    tempString = removeComment(tempString, fileSize);
    for (i = 0; i < count - 1; i++) {
        fprintf(tmpFile, "%c", tempString[i]);
    }

    free(tempString);

    fclose(tmpFile);
    fseek(file, 0, SEEK_SET);
} /* noCommentFile */

char * removeComment(char * currentString, long size) {
    long i;
    char inComment = FALSE;

    for (i = 0; i < size - 2; i++) {
        if (currentString[i] == '/' && currentString[i + 1] == '*') {
            inComment = TRUE;
        }
        if (currentString[i] == '*' && currentString[i + 1] == '/') {
            inComment = FALSE_NEXT_TWO;
        }

        if (inComment == TRUE) {
            if (currentString[i] != '\r' && currentString[i] != '\n') {
                currentString[i] = '`';
            }
        } else if (inComment == FALSE_NEXT_TWO) {
            if (currentString[i] != '\r' && currentString[i] != '\n') {
                currentString[i] = '`';
            }
            inComment = FALSE_NEXT;
        } else if (inComment == FALSE_NEXT) {
            if (currentString[i] != '\r' && currentString[i] != '\n') {
                currentString[i] = '`';
            }
            inComment = FALSE;
        }
    }
    inComment = FALSE;
    for (i = 0; i < size - 2; i++) {
        if (currentString[i] == '/' && currentString[i + 1] == '/' && currentString[i - 1] != '"') {
            inComment = TRUE;
        }
        if (currentString[i] == '\r' || currentString[i] == '\n') {
            inComment = FALSE;
        }
        if (inComment == TRUE) {
            currentString[i] = '`';
        }
    }
    return currentString;
} /* removeComment */
