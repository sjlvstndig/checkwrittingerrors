/**
 * @Author: Luis Valdez,Bastien Nisole,Patrick Zhuang
 * @Date:   2018-11-09T23:44:52+01:00
 * @Email:  patrickzhuang@hotmail.fr
 * @Project: Linter-c
 * @Filename: integrity.c
 * @Last modified by:   Luis Valdez
 * @Last modified time: 2018-12-11T03:00:20+01:00
 */


#include "headers.h"

int checkIntegrity(FILE * confFile) {
    fseek(confFile, 0, SEEK_SET);
    char string[200];
    char currentStep = NO_STEP;
    while (fgets(string, 200, confFile) != NULL) {
        trimString(string);
        if (string[0] == '=') {
            if (!strcmp(string, EXTENDS_HEADER)) {
                currentStep = EXTENDS_STEP;
                continue;
            } else if (!strcmp(string, EXCLUDEDFILES_HEADER)) {
                currentStep = EXCLUDEDFILES_STEP;
                continue;
            } else if (!strcmp(string, RECURSIVE_HEADER)) {
                currentStep = RECURSIVE_STEP;
                continue;
            } else if (!strcmp(string, RULES_HEADER)) {
                currentStep = RULES_STEP;
                continue;
            } else {
                printf("L'entete %s n'existe pas !\n", string);
                return ERROR;
            }

            if (currentStep != NO_STEP && string[0] == '\0') {
                switch (currentStep) {
                    case EXTENDS_STEP:
                        if (!checkfilePathIntegrity(string)) {
                            printf("Les chemins des fichiers a extends sont erroné ! \n");
                            return ERROR;
                        }
                        break;
                    case EXCLUDEDFILES_STEP:
                        if (!checkfilePathIntegrity(string)) {
                            printf("Les chemins des fichiers a exclure sont erroné ! \n");
                            return ERROR;
                        }
                        break;
                    case RECURSIVE_STEP:
                        if (!checkRecursiveIntegrity(string)) {
                            printf("La valeur recursive est inexacte (true OU false) \n");
                            return ERROR;
                        }
                        break;
                    case RULES_STEP:
                        if (!checkRulesIntegrity(string)) {
                            return ERROR;
                        }
                        break;
                }
            }
        }
    }

    fseek(confFile, 0, SEEK_SET);

    return SUCCESS;
} /* checkIntegrity */

int checkRulesIntegrity(char * rule) {
    return 1;
}

int checkRecursiveIntegrity(char * integrity) {
    if (!strcmp(integrity, TRUE_STRING) || !strcmp(integrity, FALSE_STRING)) {
        return TRUE;
    }

    return FALSE;
}

int checkfilePathIntegrity(char * excludedFile) {
    if (excludedFile[0] == '-') {
        return TRUE;
    }

    return FALSE;
}
