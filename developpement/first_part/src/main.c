/**
 * @Author: Luis Valdez,Bastien Nisole,Patrick Zhuang
 * @Date:   2018-11-01T12:10:49+01:00
 * @Email:  luis@valdez.fr
 * @Project: Linter-c
 * @Filename: main.c
 * @Last modified by:   Luis Valdez
 * @Last modified time: 2018-12-11T03:00:25+01:00
 */


#include "headers.h"

int main(int argc, char ** argv) {
    Linter * linter;
    FILE * errorLog = fopen("integrityError.txt", "wb");

    fclose(errorLog);

    linter = getConfig();

    if (linter == NULL) {
        return EXIT_FAILURE;
    } else {
        if (checkWrittingRules(linter) == ERROR) {
            printf("Failure.");
            destroyLinter(linter);
            return EXIT_FAILURE;
        }
        if (parseFuncFiles(linter) == ERROR) {
            printf("Failure.");
            destroyLinter(linter);
            return EXIT_FAILURE;
        }
        destroyLinter(linter);
    }
    // printErrors();
    printf("Success.");
    return EXIT_SUCCESS;
} /* main */
