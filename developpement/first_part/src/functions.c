/**
 * @Author: Luis Valdez
 * @Date:   2018-12-09T20:50:59+01:00
 * @Email:  luis@valdez.fr
 * @Project: Linter-c
 * @Filename: Functions.c
 * @Last modified by:   Luis Valdez
 * @Last modified time: 2018-12-11T03:00:08+01:00
 */

#include "headers.h"


Function * destroyFunc(Function * func) {
    if (func == NULL) return func;

    if (func->name != NULL) free(func->name);

    if (func->type != NULL) free(func->type);
    if (func->content != NULL) free(func->content);
    if (func->paramsString != NULL) free(func->paramsString);

    free(func);
    return NULL;
}

Function * newFunc() {
    Function * func = malloc(sizeof(Function));

    if (func == NULL) return NULL;

    func->type         = NULL;
    func->name         = NULL;
    func->content      = NULL;
    func->paramsString = NULL;
    func->initPosition = 0;
    func->endPosition  = 0;

    return func;
}

Functions * newArrayFunc() {
    Functions * func = malloc(sizeof(Functions));

    if (func == NULL) return NULL;

    func->funcs = NULL;
    func->size  = 0;

    return func;
}

Functions * destroyArrayFunc(Functions * func) {
    if (func == NULL) return func;

    int index;
    for (index = 0; index < func->size; index++) {
        func->funcs[index] = destroyFunc(func->funcs[index]);
    }


    free(func->funcs);

    free(func);

    return NULL;
}

int addFunc(Functions * funcs, Function * value) {
    if (value == NULL || funcs == NULL || funcs->funcs == NULL || funcs->size == 0) return 0;

    int i;
    Function ** newFuncs = malloc(sizeof(Function *) * (funcs->size + 1));
    if (newFuncs == NULL) NULL;
    for (i = 0; i < funcs->size; i++) {
        newFuncs[i] = cpyFunc(funcs->funcs[i]);

        destroyFunc(funcs->funcs[i]);
    }

    free(funcs->funcs);

    funcs->funcs = newFuncs;

    funcs->funcs[funcs->size] = cpyFunc(value);

    funcs->size = funcs->size + 1;

    return 1;
}

Function * cpyFunc(Function * value) {
    Function * cpy = malloc(sizeof(Function));

    if (cpy == NULL) return NULL;


    if (value->name != NULL) {
        cpy->name = malloc(strlen(value->name) + 1);

        strcpy(cpy->name, value->name);
    }

    if (value->type != NULL) {
        cpy->type = malloc(strlen(value->type) + 1);
        strcpy(cpy->type, value->type);
    }

    if (value->content != NULL) {
        cpy->content = malloc(strlen(value->content) + 1);
        strcpy(cpy->content, value->content);
    }

    if (value->paramsString != NULL) {
        cpy->paramsString = malloc(strlen(value->paramsString) + 1);
        strcpy(cpy->paramsString, value->paramsString);
    }

    cpy->initPosition = value->initPosition;
    cpy->endPosition  = value->endPosition;

    return cpy;
} /* cpyFunc */
