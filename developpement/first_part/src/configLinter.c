/**
 * @Author: Luis Valdez,Bastien Nisole,Patrick Zhuang
 * @Date:   2018-11-01T12:10:49+01:00
 * @Email:  luis@valdez.fr
 * @Project: Linter-c
 * @Filename: configLinter.c
 * @Last modified by:   Luis Valdez
 * @Last modified time: 2018-12-11T03:00:05+01:00
 */


#include "headers.h"

/**
 * get the rules from one file
 *
 * @method getRules
 *
 * @param  source   config file where you need to get rules
 * @param  linter   linter struct where the rules will be stocked
 *
 * @return          1 if sucess and 0 if error
 */
int getRules(FILE * source, Linter * linter) {
    char * rule     = malloc(50);
    char rulesFound = 0;

    if (rule == NULL || source == NULL || linter == NULL || linter->rules == NULL) return 0;

    while (fgets(rule, 50, source) != NULL) {
        trimString(rule);
        if (!strcmp(rule, RULES_HEADER)) {
            rulesFound = 1;
        } else if (rulesFound) {
            if (rule[0] == '=')
                break;
            if (rule[0] == 0)
                continue;
            identifyRule(rule, linter);
        }
    }

    if (!rulesFound) return 0;

    return 1;
}

/**
 * get Recurive value in conf file
 *
 * @method getRecursiveConfig
 *
 * @param  conf               file where we need to check the value of recurive
 * @param  linter             linter struct where we will stock the value (linter->recursive)
 *
 * @return                    1 if sucess and 0 if error
 */
int getRecursiveConfig(FILE * conf, Linter * linter) {
    char * value    = malloc(50);
    char rulesFound = 0;

    fseek(conf, 0, SEEK_SET);

    while (fgets(value, 50, conf) != NULL) {
        trimString(value);

        if (!strcmp(value, RECURSIVE_HEADER)) {
            rulesFound = 1;
        } else if (rulesFound) {
            if (value[0] == '=')
                break;
            if (value[0] == 0)
                continue;
        }
    }

    if (!strcmp(value, TRUE_STRING)) {
        linter->recursive = 1;
        fseek(conf, 0, SEEK_SET);
        return 1;
    } else if (!strcmp(value, FALSE_STRING)) {
        linter->recursive = 0;
        fseek(conf, 0, SEEK_SET);
        return 1;
    }
    fseek(conf, 0, SEEK_SET);

    return 0;
} /* getRecursiveConfig */

/**
 * get value depending on its type
 *
 * @method getValue
 *
 * @param  value    the string which contains the value
 * @param  type     the type of value
 *
 * @return          return value
 */
int getValue(char * value, char type) {
    if (type == INTEGER_VALUE) {
        if (!strcmp(value, "off")) {
            return -1;
        } else {
            return atoi(value);
        }
    } else if (type == NOT_INTEGER_VALUE) {
        if (!strcmp(value, "on")) {
            return ON;
        } else {
            return OFF;
        }
    }

    return OFF;
}

/**
 * set the value of a rule thanks to a config line
 *
 * @method identifyRule
 *
 * @param  rule         the config line in rules section
 * @param  linter       the struct linter where you want to stock the value (linter->rules->*)
 *
 * @return              0 if rule don't be reconized or if there are errors and 1 if success
 */
int identifyRule(char * rule, Linter * linter) {
    char * tag = rule, * value = rule;

    while (*(value++) != '=');
    while (*(++tag) != '=');
    *tag = 0;
    tag  = rule;
    tag++;
    // printf("tag : %s| and value %s|\n", tag, value);

    if (!strcmp(tag, "array-bracket-eol")) {
        linter->rules->arrayBracketEol = getValue(value, NOT_INTEGER_VALUE);
    } else if (!strcmp(tag, "operators-spacing")) {
        linter->rules->operatorsSpacing = getValue(value, NOT_INTEGER_VALUE);
    } else if (!strcmp(tag, "comma-spacing")) {
        linter->rules->commaSpacing = getValue(value, NOT_INTEGER_VALUE);
    } else if (!strcmp(tag, "indent")) {
        linter->rules->indent = getValue(value, INTEGER_VALUE);
    } else if (!strcmp(tag, "comments-header")) {
        linter->rules->commentsHeader = getValue(value, NOT_INTEGER_VALUE);
    } else if (!strcmp(tag, "max-line-numbers")) {
        linter->rules->maxLineNumbers = getValue(value, INTEGER_VALUE);
    } else if (!strcmp(tag, "max-file-line-numbers")) {
        linter->rules->maxFileLineNumbers = getValue(value, INTEGER_VALUE);
    } else if (!strcmp(tag, "no-trailing-spaces")) {
        linter->rules->noTrailingSpaces = getValue(value, NOT_INTEGER_VALUE);
    } else if (!strcmp(tag, "no-multi-declaration")) {
        linter->rules->noMultiDeclaration = getValue(value, NOT_INTEGER_VALUE);
    } else if (!strcmp(tag, "unused-variable")) {
        linter->rules->unusedVariable = getValue(value, NOT_INTEGER_VALUE);
    } else if (!strcmp(tag, "undeclared-variable")) {
        linter->rules->undeclaredVariable = getValue(value, NOT_INTEGER_VALUE);
    } else if (!strcmp(tag, "no-prototype")) {
        linter->rules->noPrototype = getValue(value, NOT_INTEGER_VALUE);
    } else if (!strcmp(tag, "unused-function")) {
        linter->rules->unusedFunction = getValue(value, NOT_INTEGER_VALUE);
    } else if (!strcmp(tag, "undeclared-function")) {
        linter->rules->undeclaredFunction = getValue(value, NOT_INTEGER_VALUE);
    } else if (!strcmp(tag, "variable-assignment-type")) {
        linter->rules->variableAssignmentType = getValue(value, NOT_INTEGER_VALUE);
    } else if (!strcmp(tag, "function-parameters-type")) {
        linter->rules->functionParametersType = getValue(value, NOT_INTEGER_VALUE);
    } else {
        return 0;
    }
    return 1;
} /* identifyRule */
