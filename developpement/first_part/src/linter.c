/**
 * @Author: Luis Valdez,Bastien Nisole,Patrick Zhuang
 * @Date:   2018-11-01T12:10:49+01:00
 * @Email:  luis@valdez.fr
 * @Project: Linter-c
 * @Filename: linter.c
 * @Last modified by:   Luis Valdez
 * @Last modified time: 2018-12-11T03:00:23+01:00
 */

#include "headers.h"

/**
 * set a new Linter structure
 *
 * @method newLinter
 *
 * @return the new Linter structure
 */
Linter * newLinter() {
    Linter * linter = malloc(sizeof(Linter));

    linter->rules         = malloc(sizeof(Rules));
    linter->includedFiles = malloc(sizeof(ArrayInfo));
    linter->recursive     = 0;

    linter->rules->arrayBracketEol  = -1;
    linter->rules->operatorsSpacing = -1;
    linter->rules->commaSpacing     = -1;
    linter->rules->indent                 = -1;
    linter->rules->commentsHeader         = -1;
    linter->rules->maxLineNumbers         = -1;
    linter->rules->maxFileLineNumbers     = -1;
    linter->rules->noTrailingSpaces       = -1;
    linter->rules->noMultiDeclaration     = -1;
    linter->rules->unusedVariable         = -1;
    linter->rules->undeclaredVariable     = -1;
    linter->rules->noPrototype            = -1;
    linter->rules->unusedFunction         = -1;
    linter->rules->undeclaredFunction     = -1;
    linter->rules->variableAssignmentType = -1;
    linter->rules->functionParametersType = -1;
    return linter;
}

/**
 * free a Linter Structure
 *
 * @method destroyLinter
 *
 * @param  linter        the Linter structure that you want to destroy
 *
 * @return               return 1 if success and else 0
 */
int destroyLinter(Linter * linter) {
    if (linter == NULL) return 0;

    if (linter->includedFiles != NULL) {
        free2dArray(linter->includedFiles->fileToCheck, linter->includedFiles->size - 1);
        free(linter->includedFiles);
    }

    if (linter->rules != NULL) {
        free(linter->rules);
    }


    free(linter);
    return SUCCESS;
}
